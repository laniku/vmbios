.code16

.macro  ROM_CALL addr
        mov  sp, offset 1f
        jmp  \addr
1:
.endm

SUPERIO_BASE  = 0x398
PC97338_FER   = 0x00
PC97338_FAR   = 0x01
PC97338_PTR   = 0x02
COM_BASE      = 0x3f8
COM_RB        = 0x00
COM_TB        = 0x00
COM_BRD_LO    = 0x00
COM_BRD_HI    = 0x01
COM_IER       = 0x01
COM_FCR       = 0x02
COM_LCR       = 0x03
COM_MCR       = 0x04
COM_LSR       = 0x05

.section begin, "a"
        .ascii "VMX"
        .align 16

.section main, "ax"
.globl init
init:
        cli
        cld
        mov  ax, cs
        mov  ds, ax
        mov  ss, ax
 
init_superio:
        mov  dx, SUPERIO_BASE
        in   al, dx
        in   al, dx
        mov  si, offset superio_conf
        mov  cx, (serial_conf - superio_conf)/2

write_superio_conf:
        mov  ax, [si]
        ROM_CALL superio_out
        add  si, 0x02
        loop write_superio_conf
 
init_serial:
        mov  si, offset serial_conf
        mov  cx, (hello_string - serial_conf)/2
write_serial_conf:
        mov  ax, [si]
        ROM_CALL serial_out
        add  si, 0x02
        loop write_serial_conf
 
print_ver:
        mov  si, offset ver_string
        ROM_CALL print_string
 
serial_repeater:
        ROM_CALL readchar
        ROM_CALL putchar
        jmp serial_repeater

[ORG 0x7c00]
 
start:   xor ax, ax
   mov ds, ax
   mov ss, ax
   mov sp, 0x9c00
 
   cli
   push ds
 
   lgdt [gdtinfo]
 
   mov  eax, cr0
   or al,1
   mov  cr0, eax
 
   mov  bx, 0x08
   mov  ds, bx
 
   and al,0xFE
   mov  cr0, eax
 
   pop ds
   sti
 
   mov bx, 0x0f01
   mov eax, 0x0b8000
   mov word [ds:eax], bx
 
   jmp $
 
gdtinfo:
   dw gdt_end - gdt - 1
   dd gdt
 
gdt        dd 0,0
flatdesc    db 0xff, 0xff, 0, 0, 0, 10010010b, 11001111b, 0
gdt_end:
 
   times 510-($-$$) db 0
   db 0x55
   db 0xAA
 hang:
     jmp hang
 
     times 510-($-$$) db 0
     db 0x55
     db 0xAA

superio_conf:
        .byte PC97338_FER, 0x0f
        .byte PC97338_FAR, 0x10
        .byte PC97338_PTR, 0x00
serial_conf:
        .byte COM_MCR,     0x00
        .byte COM_FCR,     0x07
        .byte COM_LCR,     0x80
        .byte COM_BRD_LO,  0x01
        .byte COM_BRD_HI,  0x00
        .byte COM_LCR,     0x03
ver_string:
        .string "\r\nLaniku's x86 bios v1!\r\n"